module.exports = (function () {
  return {
    xen: [
      {
        host: "Secret",
        port: "3306",
        user: "root",
        pass: "Secret"
      }
    ],
    http: {
      https_port: 444,
      hostname: "loopback.rpgfarm.kr",
      port_autobind: false,
      key: "Secret!"
    }
  }
})();
